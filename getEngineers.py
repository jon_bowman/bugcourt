import csv
'''
    Parse a csv file into a dictionary
'''


class Person(object):
    def __init__(self, name, team, projects, knowledge, desk, contact, picture, email, phone):
        self.name = name
        self.team = team
        self.projects = projects
        self.knowledge = knowledge
        self.desk = desk
        self.contact = contact
        self.picture = picture
        self.email = email
        self.phone = phone

    def getName(self):
        return self.name

    def getTeam(self):
        return self.team

    def getKnowledge(self):
        return self.knowledge

    def getProjects(self):
        return self.projects

    def getDesk(self):
        return self.desk

    def getContact(self):
        return self.contact

    def getPicture(self):
        return self.getPicture

    def getEmail(self):
        return self.email

    def getPhone(self):
        return self.phone

    def __str__(self):
        return self.name





def queryEngineers(rosterFile):
    print "Querying Engineers now ....."
    f = open(rosterFile)
    csv_f = csv.reader(f)
    engD = {}

    for row in csv_f:
        engD[row[0]] = Person(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8])


    return engD