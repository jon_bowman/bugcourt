fbURL = "https://solidfire.fogbugz.com/f/cases/edit/"

'''
    Name:   getengineerReport

    Desc:   Given an engineer return a formatted report in the form of a list of strings

    Input:  Path to the metricsData directory

    Returns:    a list of formatted strings that conatins all the bugs an engineer owns.
'''

def getengineerReport(metricDir, engbugDict, bugDict, engineerDict, SECTDIVIDER, key):
    msgList = []
    line = 0

    msgList.append("%s" % "-" * SECTDIVIDER)

    # loop using the bugs associated with an engineer
    for l in engbugDict[key]:
        # Create a flag that will show wether a bug is assigned to or opened by an engineer.
        if bugDict[l].getAssignedto() == key:
            engFlag = 'a'
        else:
            engFlag = 'o'
        # Convert the bug number into a URL
        urlS = "%s%s" % (fbURL, l)
        # Just for formatting we want the first line to include the engineers name and subsequent lines
        #   contain a tab in the first spot so we indent the bugs.
        if line == 0:
            msgList.append("|%20s : %s(%s)\t %s <%s>" % (key, str(urlS), engFlag, bugDict[l].getTitle(),
                                                         bugDict[l].getProject()))
            line = line + 1
        else:
            msgList.append("|%20s   %s(%s)\t %s <%s>" % (' ', str(urlS), engFlag, bugDict[l].getTitle(),
                                                         bugDict[l].getProject()))
    return msgList



# TODO need to define function for teamreport
'''
    Name:   getTeamreport

    Desc:

    Input:

    Returns:
'''
def getTeamreport(team, engineerDict, bugDict, engbugDict):
    msgList = []

    for enginTeam in engbugDict.keys():
        if engineerDict[enginTeam].getTeam() == team:
            for aBug in engbugDict[enginTeam]:
                if bugDict[aBug].getAssignedto() == enginTeam:
                    engFlag = 'a'
                else:
                    engFlag = 'o'
                msg = "%20s : %s(%s)\t%s <%s>" % (enginTeam , aBug, engFlag, bugDict[aBug].getTitle(), bugDict[aBug].getProject())
                msgList.append(msg)

    return msgList







'''
    Name:   printaList

    Desc:   Takes a list and prints each member of the list

    Input:  A list

    Returns:    None
'''
def printaList(aList):
    for item in aList:
        print item
    return None


'''
    Name:   listtoStr

    Desc:   Takes a list and returns a sting with newlines for each list item

    Input:  A list

    Returns:    String
'''
def listtoStr(aList):
    myString = ''
    for item in aList:
        myString = myString + item + '\n'
    return myString

