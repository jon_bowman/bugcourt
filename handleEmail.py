import smtplib
import string
import generateReports


def actuallysendEmail(me, you, subject, msg):

    HOST = "localhost"
    body = string.join((
        "From: %s" % me,
        "To: %s" % ', ' .join(you),
        "Subject: %s" % subject,
        "",
        msg), "\r\n")

    server = smtplib.SMTP(HOST)
    server.sendmail(me, you, body)
    server.quit()



def sendbcEmail(frm, to, subject, ml, emailOption, debugOption):
    newMsg = ''
    for msg in ml:
        newMsg = newMsg + msg + "\n"


'''
    Name: emailHandling

    Desc:

        Safety check to aid debugging and avoided spam emails while testing note
        by default we set emailOption to False and debugOption to True

        emailOption     debugOption     Action
        -------------------------------------------------------------------
        true            false           send emails and keep console output to non
        true            true            send all emails to Jon only overrides the to: field
        false           true            just print the emails to the screen and go verbose
        false           false           just print the email to the screen

    Input:

    Returns:

    '''

def emailHandling(frm, to, subject, ml, ob, emailOption, debugOption):
    # The TO: field needs to be passed as a list
    TO = []
    TO.append(to)

    if not emailOption and debugOption:
        generateReports.printaList(ml)

    elif emailOption and  debugOption:
        # for this we override the to field to debug and check that real emails can be sent.
        print "DEBUG: Email"
        print "from:\t\t" + frm
        print "to:\t\t" + generateReports.listtoStr(TO)
        print "subject:\t\t" + subject
        generateReports.printaList(ml)
        if to != '':
            TO = [ "jon.bowman@netapp.com" ]
            actuallysendEmail(frm, TO, subject, generateReports.listtoStr(ml))
        else:
            print "\n\nno email address found for  %s cannot send email\n\n" % (ob)

    elif emailOption and not debugOption:
        if to != '':
            # TODO when we are happy with this we can remove the below appends which add the devs to the email
            TO.append("jon.bowman@netapp.com")
            TO.append("carlos.garcia@netapp.com")
            actuallysendEmail(frm, TO, subject, generateReports.listtoStr(ml))
        else:
            print "\n\nno email address found for  %s cannot send email\n\n" % (ob)

    elif not emailOption and not debugOption:
        print "Email"
        print "from:\t\t" + frm
        print "to:\t\t" + generateReports.listtoStr(TO)
        print "subject:\t\t" + subject
        generateReports.printaList(ml)

