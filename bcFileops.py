import os, os.path
import errno
import argparse, sys


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise


'''
    Name : readsetQuery()

    Desc : This function reads in a query stored in a file. It gets rid of the escapes using
            decode and cleans up extra quotes etc.

    Returns : A string that contains a FogBugz suitable query
'''
def readsetQuery(args):
    queryStr = ""
    print "Query being read from " + args.queryOption
    queryHandle = open(args.queryOption, 'r')
    for line in queryHandle:
        queryStr = queryStr + line.decode('string_escape').replace("'", '')
    queryHandle.close()
    return queryStr


'''
    Name : writeData()

    Desc :

    Returns :
'''
def writeData(toWrite, datatoWrite, dateStamp, metricDir):
    dirName = metricDir + dateStamp
    mkdir_p(dirName)
    fileName = dirName + "/" + toWrite
    dataHandler = open(fileName, "w")
    for data in datatoWrite:
        data = data + '\n'
        dataHandler.writelines(data)
    dataHandler.close()
