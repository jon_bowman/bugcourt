#!/usr/bin/python


import fogbugz


class Bug(object):
    def __init__(self, bug, title, openedby, assignedto, resolvedby, priority, milestone, tags, status, project, sfaccount, sfcase ):
        self.bug = bug
        self.title = title
        self. openedby = openedby
        self.assignedto = assignedto
        self.resolvedby = resolvedby
        self.priority = priority
        self.milestone = milestone
        self.tags = tags
        self.status = status
        self.project = project
        self. sfaccount = sfaccount
        self. sfcase = sfcase


    def getBug(self):
        return self.bug

    def getTitle(self):
        return self.title

    def getOpenedby(self):
        return self.openedby

    def getAssignedto(self):
        return self.assignedto

    def getResolvedby(self):
        return self.resolvedby

    def getPriority(self):
        return self.priority

    def getMilestone(self):
        return self.milestone

    def getTags(self):
        return self.tags

    def getStatus(self):
        return self.status

    def getProject(self):
        return self.project

    def getSFaccount(self):
        return self.sfaccount

    def getSFcase(self):
        return self.sfcase


S_FOGBUGZ_URL = 'https://solidfire.fogbugz.com/'
S_EMAIL = 'autouser@solidfire.com'
S_PASSWORD = 'solidfire'

COLUMNS = ['sCategory',
           'ixBug',
           'sTitle',
           'ixPersonOpenedBy',
           'ixPersonAssignedTo',
           'ixPersonResolvedBy',
           'ixPriority',
           'sMilestone',
           'tags',
           'ixStatus',
           'ixProject',
           'salesforcexaccountxname',
           'salesforcexcasexxk21']


##This function translates the person Id with name (NOTE: It can be a little bit slow)
def find_person_api(fb, person_id):
    """Method to find the name a person based on an id.
    Returns the string.
    :param int, str person_id: id of the person from fogbugz
    :return: str
    """
    if person_id in [0, 1, '0', '1']:
        return 'None'
    person_xml = fb.viewPerson(ixPerson=person_id)
    return person_xml.find('sFullName').getText()

def find_project_api(fb, project_id):
    project_xml = fb.viewProject(ixProject=project_id)
    return project_xml.find('sProject').getText()


def queryBugs(aQuery):
    fb = fogbugz.FogBugz(S_FOGBUGZ_URL)
    fb.logon(S_EMAIL, S_PASSWORD)
    columns = ','.join(COLUMNS)

    print "Querying Fogbugz now..."
    resp = fb.search(q=aQuery, cols=columns, max=1000)

    bugDict = {}
    numofBugs = 0
    for case in resp.cases.findAll('case'):
            bug = case.ixBug.string
            syn = case.sTitle.text
            oby = find_person_api(fb, case.ixPersonOpenedBy.string)
            ato = find_person_api(fb, case.ixPersonAssignedTo.string)
            rby = find_person_api(fb, case.ixPersonResolvedBy.string)
            pri = case.ixPriority.string
            mil = "NOTIMPL"
            tag = case.tags.string
            sta = case.ixStatus.string
            pro = find_project_api(fb, case.ixProject.string)
            sfa = case.salesforcexaccountxname.string
            sfc = case.salesforcexcasexxk21.string
            bugDict[bug] = Bug(bug, syn, oby, ato, rby, pri, mil, tag, sta, pro, sfa, sfc)
            numofBugs = numofBugs + 1


    return bugDict