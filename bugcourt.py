#!/usr/bin/python

import getEngineers
import getBugs
import handleEmail
import generateReports

from bcFileops import  writeData, readsetQuery
import argparse
import datetime

global SECTDIVIDER
global engbugDict
global teambugDict
global listofBugs
global dateStamp
global emailOption
global debugOption
global fromAddress

# A test comment

SECTDIVIDER = 150
engbugDict = {}
teambugDict = {}
listofBugs = []
dateStamp  = datetime.datetime.now().ctime().replace(' ', '')
validEmailteams = ["HW Platform", "SW Platform"]
fromAddress = "jon.bowman@netapp.com"

'''
   build_engineer_lists() does the grunt work of building the dictionaries that list the bugs
        found for engineers and their teams.

    engbugDict : is a dictionary of the form of key = engineer name
                                                value = list of bug numbers

    teambugDict : is a dictionary of the form of    key = team name
                                                    value = list of bug numbers

    listofBugs : is a dynamic list built up through execution that lists the bugs found for
                    the run. These are assigned to the engbugDict or assigned or extend the
                    list of bugs for a team.

'''


def build_engineer_list():
    print "Building Engineer Lists ......"
    for anEngineer in engineerDict.keys():
        team = engineerDict[anEngineer].getTeam()
        del listofBugs[:]

        for aBug in bugDict.keys():
            added = 0
            if bugDict[aBug].getAssignedto() == anEngineer:
                listofBugs.append(str(aBug))
                added = 1

            if bugDict[aBug].getOpenedby() == anEngineer and added == 0:
                listofBugs.append(str(aBug))

        # Only add the list to the dictionary if we actually found an engineer with bugs
        if listofBugs != []:
            engbugDict[anEngineer] = list(listofBugs)
            # If the team already has bugs attached to it then append the new bugs to the list
            if team in teambugDict:
                listofBugs.extend(list(teambugDict[team]))
                teambugDict[ team ] = list(listofBugs)
            else:
                teambugDict[ team ] = list(listofBugs)


# ====================================================================================
# ===================  MAIN  =========================================================
# ====================================================================================

'''
    Define the command line arguments :

        -q || --query   : path and filename to a file containing a FB Query
        -r || --roster  : path and filename to a csv file containing engineers
        -e || --email   : a true false switch to indicate if we should email or not
        -d || --debug   : turn debug on

'''
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-q', '--query', action="store", dest="queryOption",
                            help="Location for a FB query in a file",
                            default="/Users/jbowman-LT/Development/SourceTree/bugcourt/BugCourtConfig/Query.dat")
    parser.add_argument('-r', '--roster', action="store", dest="rosterOption",
                            help="Location of a csv file from the Engineering Roster Spreadsheet",
                            default="/Users/jbowman-LT/Development/SourceTree/bugcourt/BugCourtConfig/roster.csv")
    parser.add_argument('-m', '--metrics', action="store", dest="metricOption",
                            help="Loaction of directory to store metric data",
                            default="/Users/jbowman-LT/Development/SourceTree/bugcourt/BugCourt-Runs/")
    parser.add_argument('-e', '--email', action="store_true", dest="emailOption", default=False,
                            help="Only with this option on and the debug flag turned off will emails be sent")
    parser.add_argument('-d', '--debug', action="store_true", dest="debugOption", default=False,
                            help="Turn debug statements on.")
    parser.add_argument('-f', '--fullreport', action="store_true", dest="fullOption", default=False)
    parser.add_argument('-g', '--debugemail', action="store")

    args = parser.parse_args()

    fbQuery = readsetQuery(args)
    debugOption = args.debugOption
    emailOption = args.emailOption
    rosterFile = args.rosterOption
    metricDir = args.metricOption

    if debugOption:
        print "\n%s\nfbquery = %s" % ("-" * SECTDIVIDER, fbQuery)
        print "roster file is " + rosterFile
        print "metrics data is " + metricDir
        print "debug option is  " + str(debugOption)
        print "email option is %s\n%s" % (str(emailOption), "-" * SECTDIVIDER)


'''
    Run the initial queries to build a dictionary for bugs and engineers.
    bugDict is a dictionary keyed on bug number and with a value of a Bug Object.
    engineerDict is a dictionary keyed on engineer name and with a value of a Person Object

    Usage:
        print bugDict["19466"].getAssignedto()
        print engineerDict["Jon Bowman"].getContact()

'''
engineerDict = getEngineers.queryEngineers(rosterFile)
bugDict = getBugs.queryBugs(fbQuery)

'''
    Populate two lists one with a list of bugs and data per engineer and the other with a
    list of bugs and data per team
'''
build_engineer_list()

'''
    Run the report per engineer that shows the engineers bugs as well as his teams.
    we get back a list of stings

    for each engineer build a report
    engbugDict has keys of an engineers name
'''
for key in engbugDict.keys():
    eReport = generateReports.getengineerReport(metricDir, engbugDict, bugDict, engineerDict, SECTDIVIDER, key)

    # writeData stores the bugs per engineer in a timestamped directory
    writeData(key, engbugDict[key], dateStamp, metricDir)

    # Add some header lines and the engineers team name to msglist
    eReport.append("%s" % ("-" * SECTDIVIDER))
    engTeam = engineerDict[key].getTeam()
    eReport.append("%20s Team Report" % engTeam)
    eReport.append("%s" % ("-" * SECTDIVIDER))
    tReport = generateReports.getTeamreport(engTeam, engineerDict, bugDict, engbugDict)
    eReport.append(generateReports.listtoStr(tReport))

    if engTeam in validEmailteams:
        to = engineerDict[key].getEmail()
        subject = "BugCourt Report for Engineer %s" % key
        handleEmail.emailHandling(fromAddress, to, subject, eReport, key, emailOption, debugOption)

'''
    Now build team lists these are really just for the managers and we expect Manager to be in the roster.

    The logic here is loop over every team we found then loop over very person found and set the
        email address when we find a person who is a Manager and belongs to the Team.
'''

print "\n\n\n\n\n\n\n%s" % ("+" * SECTDIVIDER)

for team in teambugDict:
    to = ""
    for person in engineerDict:
        if engineerDict[person].getTeam() == team and engineerDict[person].getProjects() == "Manager":
            to = engineerDict[person].getEmail()
    subject = "%s Team BugCourt Report for manager %s" % (team, to)

    if debugOption:
        print "\n\n%s %s Team Report for Manager %s %s" % ("-" * (SECTDIVIDER/2), team, to,
                                                           "-" * ((SECTDIVIDER/2)-len(" Team Report for Manager " +
                                                                                      team + to)))

    teamList = generateReports.getTeamreport(team, engineerDict, bugDict, engbugDict)

    handleEmail.emailHandling(fromAddress, to, subject, teamList, team, emailOption, debugOption)

print "%s" % ("+" * SECTDIVIDER)

